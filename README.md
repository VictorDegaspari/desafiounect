# ![logo](https://user-images.githubusercontent.com/61597038/85148313-5e5ee480-b226-11ea-8d20-cf3bf7bcd11d.png)

# Objetivos
- Informa ao cliente a proposta da empresa Unect Jr.
- Possui uma area para o cliente cadastrar suas informacoes caso queira o servico.
- Uma galeria que mostra com mais detalhes a equipe, feito com JavaScript.

## Estilo e Aprendizados com CSS3
- Substituição de imagens 
- Mudanças de cor de fundo 
- Efeitos com a imagem de fundo 

## JavaScript/Elementos do DOM 
- Salvamento local de pequenas coisas para não afetar o banco de dados 
- Arrays 
- Complementando palavras para facilitar o cliente
- Definindo horário de funcionamento
- Contagem de projetos automática
- Função de Login

